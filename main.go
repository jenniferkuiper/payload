package main

import (
	"fmt"
	"os"
)

func main() {
	var input string
	writePayloadHeader()

	for true {
		fmt.Printf("\nType the next Payload Tracker command and press <Enter>\n>> ")
		_, err := fmt.Scanf("%s", &input)
		if err != nil {
			if err.Error() == "unexpected newline" {
				infoMessage("Please enter a command.")
				continue
			}
			os.Exit(1)
		}

		if evaluateCommand(&input) == COMMAND_EXIT {
			fmt.Println("\nThank you for using Payload Tracker (c)")
			break
		}
	}
}
