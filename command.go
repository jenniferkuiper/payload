package main

import (
	"strings"
)

func evaluateCommand(command *string) int8 {
	var commandToEval string = strings.TrimSpace(strings.ToLower(*command))

	if commandToEval == "exit" || commandToEval == "quit" {
		return COMMAND_EXIT
	} else if commandToEval == "help" || commandToEval == "?" {
		Help()
	} else {
		errorMessage("I don't understand \"" + *command + "\", please elaborate.")
		Help()
	}

	return COMMAND_VOID
}
