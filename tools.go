package main

import (
	"./color"
	"fmt"
)

const BUILD_VERSION string = "0.0.0.2"
const BUILD_DATE string = "June 7th, 2021"

func writePayloadHeader() {
	fmt.Println("+++++++++++")
	fmt.Println("++++++++++++")
	fmt.Println("++        ++")
	fmt.Println("++        ++  ++++++++++  ++      ++  ++          ++++++++++  ++++++++++  ++++++++     |T|")
	fmt.Println("++++++++++++  ++++++++++   ++    ++   ++          ++++++++++  ++++++++++  ++++++++++   |R|")
	fmt.Println("+++++++++++   ++      ++    ++  ++    ++          ++      ++  ++      ++  ++      ++   |A|")
	fmt.Println("++            ++++++++++     ++++     ++          ++      ++  ++++++++++  ++      ++   |C|")
	fmt.Println("++            ++++++++++      ++      ++          ++      ++  ++++++++++  ++      ++   |K|")
	fmt.Println("++            ++      ++     ++       ++++++++++  ++++++++++  ++      ++  ++++++++++   |E|")
	fmt.Println("+             ++      ++    ++        ++++++++++  ++++++++++  ++      ++  ++++++++     |R|")
	fmt.Println()
	fmt.Println("Payload Tracker (c)")
	fmt.Println("Version " + BUILD_VERSION)
	fmt.Println("Build Date: " + BUILD_DATE)
	fmt.Println("Copyright 2021 (c) main() Development")
}

func infoMessage(text string) {
	fmt.Printf(color.Yellow)
	fmt.Printf("%s\n", text)
	fmt.Printf(color.Reset)
}

func errorMessage(text string) {
	fmt.Printf(color.Red)
	fmt.Printf("%s\n", text)
	fmt.Printf(color.Reset)
}
