======================================================================================================================
Applicatie      : Payload Tracker
Versie          : 0.0.0.2
Build Date      : 9 juni 2021
Bugfix          : -
Features        : -
Opmerkingen     : - Onboarding...
======================================================================================================================
======================================================================================================================
Applicatie      : Payload Tracker
Versie          : 0.0.0.1
Build Date      : 6 juni 2021
Bugfix          : -
Features        : -
Opmerkingen     : - Start van de Payload tracker, een CLI om door een (WSO2) service heen te steppen, waarbij
                    de "payload" altijd zichbaar is.
======================================================================================================================