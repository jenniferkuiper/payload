package main

import "fmt"

func Help() {
	fmt.Println()
	fmt.Println("Manage your way around in Payload Tracker (c)")
	fmt.Println("\texit\tLeave Payload Tracker (c)")
	fmt.Println("\tquit")
	fmt.Println("\thelp\tShow the Help (this) Screen")
}
